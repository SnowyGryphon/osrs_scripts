package com.snowygryphon.osrs_scripts;

import com.snowygryphon.osrs.script.*;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.util.Optional;

@ScriptInfo(name = "WC West Varrock", author = "Snowy Gryphon", version = "1.0")
public class WC_West_Varrock extends Script {
    private State state = State.Started;

    private long last_action_time = 0;

    private Vector2i last_walk_location;

    private static final Vector2i BANK_TILE = new Vector2i(3183, 3436);
    private static final Vector2i WC_LOCATION_TILE = new Vector2i(3170, 3422);
    
    private enum State {
        Started,
        GoingToBank,
        Banking,
        GoingToLocation,
        WoodCutting
    }

    private void setState(State s) {
        bot.logInfo("New State: " + s.name());
        state = s;
    }

    @Override
    public void update() {
        if(System.currentTimeMillis() + 2000 < last_action_time) {
            return;
        }
        if(bot.getLocalPlayer() == null) return;

        switch (state) {
            case Started:
                update_started(); break;
            case GoingToBank:
                update_going_to_bank(); break;
            case Banking:
                update_banking(); break;
            case WoodCutting:
                update_wood_cutting(); break;
            case GoingToLocation:
                update_going_to_location(); break;
            default:
                bot.logError("Unhandled enum value " + state.name());
                bot.stop();
        }
    }

    private void update_going_to_location() {
        if(WC_LOCATION_TILE.distance(bot.getLocalPlayer().getTile()) < 5) {
            setState(State.WoodCutting);
            return;
        }

        if(bot.getLocalPlayer().isMoving()) {
            if(!should_do_walk_action_again()) {
                return;
            }
        }

        walk_towards(WC_LOCATION_TILE);
    }

    private void update_wood_cutting() {
        if(bot.getInventory().getItems().size() == 28) {
            setState(State.GoingToBank);
        }

        if(bot.getLocalPlayer().getAnimation() == -1 && !bot.getLocalPlayer().isMoving()) {
            GameObject closest_tree = bot.getGameObjects().getClosest("Oak");
            if(closest_tree != null) {
                bot.interactWith(closest_tree, "Chop down");
                last_action_time = System.currentTimeMillis();
            }
        } else {
            last_action_time = System.currentTimeMillis();
        }
    }

    private void update_banking() {
        if(!bot.getBank().isOpen()) {
            NPC banker = bot.getNPCs().getClosest("Banker");
            bot.interactWith(banker, "Bank");
        } else {
            Optional<InventoryItem> item = bot.getInventory().getItems()
                    .stream()
                    .filter(i -> i.getName().equals("Oak logs"))
                    .findFirst();
            if(!item.isPresent()) {
                setState(State.GoingToLocation);
            } else {
                bot.getBank().depositAll(item.get());
                last_action_time = System.currentTimeMillis();
            }
        }
    }

    private void update_going_to_bank() {
        if(BANK_TILE.distance(bot.getLocalPlayer().getTile()) < 5) {
            setState(State.Banking);
            return;
        }

        if(bot.getLocalPlayer().isMoving()) {
            if(!should_do_walk_action_again()) {
                return;
            }
        }

        walk_towards(BANK_TILE);
    }

    private boolean should_do_walk_action_again() {
        return last_walk_location != null && bot.getLocalPlayer().getTile().distance(last_walk_location) < 5;
    }

    private void update_started() {
        if(bot.getInventory().getItems().size() == 28) {
            setState(State.GoingToBank);
        } else {
            setState(State.GoingToLocation);
        }
    }

    private boolean walk_towards(Vector2i end_point) {
        Vector2i walk_to_point = TileUtil.moveTowards(bot.getLocalPlayer().getTile(), end_point, 12);
        last_walk_location = walk_to_point;
        last_action_time = System.currentTimeMillis();
        return bot.walkToTileOnMiniMap(walk_to_point);

        /*
        Vector2i start_point = ;
        if(start_point.distance(end_point) >= 12) {
            Vector2f new_pos = new Vector2f(end_point)
                    .sub(start_point.x, start_point.y)
                    .normalize()
                    .mul(12);

            start_point.add((int)new_pos.x, (int)new_pos.y);

        } else {
            last_walk_location = end_point;
            last_action_time = System.currentTimeMillis();
            return bot.walkToTileOnMiniMap(end_point);
        }*/
    }
}
